#ifndef __COMMON_H__
#define __COMMON_H__

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <list>
#include <vector>
#include <set>
#include <ctime>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <cassert>

#if WINDOWS_VERSION
#else
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#endif

typedef unsigned char player;
const player white = 0;
const player black = 1;
const player none = 2;

typedef unsigned long long ulong64;

#define N 11
#define NXN 121
#define END (-1)
#define OO 1000000000

#define MAX_MOVES 100
#define MAX_MEMBERS 30

#endif
