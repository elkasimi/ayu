#include "common.h"

#include "group.h"
#include "position.h"
#include "move.h"
#include "data.h"
#include "queue.h"
#include "util.h"


void group::add(int f) {
	m_members.insert(f);

	for (int* n = neighbors[f]; *n != END; ++n)
		m_neighbors[*n] += 1;
}

void group::remove(int f) {
	m_members.erase(f);

	for (int* n = neighbors[f]; *n != END; ++n)
		m_neighbors[*n] -= 1;
}

void group::merge_with(const group& g) {
	for (auto it = g.m_members.begin(), end = g.m_members.end(); it != end; ++it) {
		int m = *it;
		if (!contains(m)) {
			add(m);
		}
	}
}

bool group::contains(int f) const {
	return (m_members.find(f) != m_members.end());
}

bool group::has_neighbor(int f) const {
	return (m_neighbors[f] > 0);
}

void group::get_possible_moves(move_list& moves, const position* pos) const {
	//INIT MIND = OO
	//DO A BFS SEARCH
	//ADD ELEMENT WHEN ITS DISTANCE ISS LESS THAN MIND
	//WHENEVER YOU REACH A NEW GROUP
	//UPDATE MIND
	//MARK THE ORIGIN NEIGHBOUR AS A POSSIBLE MOVE
	int mind = OO;
	bool possible[NXN];
	int d[NXN], origin[NXN];
	queue q;

	for (int f = 0; f < NXN; ++f) {
		origin[f] = -1;
		possible[f] = false;
		if (contains(f)) {
			d[f] = 0;
			q.push(f);
		}
		else {
			d[f] = OO;
		}
	}

	while (!q.empty()) {
		int t = q.pop();

		if ((origin[t] == -1) && (d[t] == 1)) {
			origin[t] = t;
		}

		if (d[t] > mind) {
			continue;
		}

		for (int* n = neighbors[t]; *n != END; ++n) {
			if (pos->is_empty(*n) && (d[*n] >= d[t] + 1)) {
				d[*n] = d[t] + 1;
				q.push(*n);
				origin[*n] = origin[t];
			}

			if ((pos->get_owner(*n) == m_owner) && !contains(*n)) {
				//we reach a group
				int o = origin[t];
				if (o != -1) {
					possible[o] = true;
				}

				//update min distance
				if (mind > d[t]) {
					mind = d[t];
				}
			}
		}
	}

	for (int a = 0; a < NXN; ++a) {
		if (!possible[a])
			continue;

		for (auto it = m_members.begin(), end = m_members.end(); it != end; ++it) {
			int m = *it;
			int from = m;
			int to = a;
			if (check_move(from, to, pos))
				moves.push_back(move(from, to));
		}
	}
}

move group::get_random_move(const position* pos) const {
	move random_move = invalid_move;

	move_list moves;
	get_possible_moves(moves, pos);
	int len = (int)moves.size();

	if (len > 0) {
		int r = rand() % len;
		random_move = moves[r];
	}

	return random_move;
}

bool group::check_move(int from, int to, const position* pos) const {
	//no check is needed for unit groups
	if (m_members.size() == 1)
		return true;

	player owner[NXN];
	pos->get_fields_owner(owner);
	owner[from] = none;
	owner[to] = m_owner;

	int members[MAX_MEMBERS];
	int count = get_members(members);
	for (int i = 0; i < count; ++i) {
		if (members[i] == from) {
			members[i] = to;
			break;
		}
	}
	
	bool connected[NXN];
	memset(connected, false, NXN);
	int m0 = members[0];
	queue q;
	q.push(m0);
	connected[m0] = true;
	while (!q.empty()) {
		int t = q.pop();
		for (int* n = neighbors[t]; *n != END; ++n){
			if (!connected[*n] && (owner[*n] == m_owner)) {
				q.push(*n);
				connected[*n] = true;
			}
		}
	}

	bool ok = true;

	for (int i = 0; i < count; ++i) {
		int m = members[i];
		if (!connected[m]) {
			ok = false;
			break;
		}
	}

	return ok;
}
