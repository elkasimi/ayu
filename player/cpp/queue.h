#ifndef __QUEUE_H__
#define __QUEUE_H__

#define MAX_QUEUE_SIZE 1024

class queue {
public:
	queue() : m_start(0), m_end(0) {}
	int pop() { return m_a[m_start++]; }
	void push(int val) { m_a[m_end++] = val; }
	bool empty() { return (m_start == m_end); }
private:
	int m_start;
	int m_end;
	int m_a[MAX_QUEUE_SIZE];
};

#endif
