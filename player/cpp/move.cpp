#include "common.h"

#include "move.h"

const move invalid_move = { 0, 0 };

std::ostream& operator <<(std::ostream& out, const move& m) {
	int from = m.from;
	int to = m.to;
	out << ((char)('A' + (from%N))) << ((from / N) + 1) << "-" << ((char)('A' + (to%N))) << ((to / N) + 1);
	return out;
}

std::istringstream& operator >>(std::istringstream& in, move& m) {
	char c1, c2, minus;
	int n1, n2;

	in >> c1 >> n1 >> minus >> c2 >> n2;
	m.from = (c1 - 'A') + N * (n1 - 1);
	m.to = (c2 - 'A') + N * (n2 - 1);

	return in;
}

bool operator != (const move& lhs, const move& rhs) {
	return (lhs.from != rhs.from) || (lhs.to != rhs.to);
}
