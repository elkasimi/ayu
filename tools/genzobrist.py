import random

print "UInt zobrist_init_value = 0x%08xu;" %random.getrandbits(32)
print ''
print "UInt zobrist_white_player = 0x%08xu;" %random.getrandbits(32)
print ''
print "UInt zobrist_black_player = 0x%08xu;" %random.getrandbits(32)
print ''
print "UInt zobrist_field[NXN][2] = {"

for i in range(121):
	print "\t{0x%08xu, 0x%08xu}," %(random.getrandbits(32), random.getrandbits(32))

print "};"
