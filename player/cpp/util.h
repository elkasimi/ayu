#ifndef __UTIL_H__
#define __UTIL_H__

#include <list>

class util {
public:
	static void init_random_seed();
	static void display_first_error();

	template <class T>
	static typename std::list<T>::iterator get_random_iterator(std::list<T>& l) {
		auto i = l.end();
		
		if (!l.empty()) {
			int len = (int)l.size();
			i = l.begin();
			int r = rand() % len;
			std::advance(i, r);
		}

		return i;
	}

	template <class T>
	static typename std::list<T>::const_iterator get_random_iterator(const std::list<T>& l) {
		auto i = l.end();
		
		if (!l.empty()) {
			int len = (int)l.size();
			i = l.begin();
			int r = rand() % len;
			std::advance(i, r);
		}

		return i;
	}	
};

#endif
