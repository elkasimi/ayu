#ifndef __TREE_H__
#define __TREE_H__

#include "move.h"

class position;
struct move;

class tree {
public:
	static tree* create(int level, const move& m, tree* parent, const position& pos);

	static void clear_allocated_trees();

	bool is_fully_expanded() const {
		return m_untried_moves.empty();
	}

	bool is_leaf() const {
		return m_is_leaf;
	}
	
	tree* expand(position& pos);
	
	tree* select() const;
	
	tree* select_most_visited_child() const;

	tree* select_random_child() const;

	tree* select_uct_child() const;
	
	double get_value() const {
		return m_value;
	}
	
	double get_uct_value() const;
	
	void update(player p, double value, int count);

	int get_children_count() const {
		return (int)m_children.size();
	}

	const move& get_move() const {
		return m_move;
	}

	tree* get_parent() const {
		return m_parent;
	}

	int get_visits() const {
		return m_visits;
	}

private:
	tree();
	tree(const tree& t);
	tree& operator =(const tree& t);
	static std::list<tree*> ms_allocated_trees;
	static double ms_uctk;

	//the move that got us to this tree
	move m_move;

	//the parent tree NULL for root tree
	tree* m_parent;

	//tree children
	std::list<tree*> m_children;

	//Monte Carlo statistics
	double m_value;
	int m_visits;

	//untried moves count
	move_list m_untried_moves;

	//player just moved
	player m_player;

	//to know tree level
	int m_level;

	//to know if tree is leaf or not
	bool m_is_leaf;
};

#endif
