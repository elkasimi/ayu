#include "common.h"

#include "ai.h"
#include "position.h"
#include "timer.h"
#include "tree.h"

int ratio = 0;

int mcts_ai::ms_max_iterations = 100000;
int mcts_ai::ms_max_samples = 30;

void mcts_ai::print_variation(const tree* root) const {
	std::cerr << "ev = ";
	bool first = true;
	for (const tree* t = root;;) {
		if (first){
			first = false;
		}
		else {
			std::cerr << "=>";
		}

		std::cerr << t->get_move();

		if (t->is_fully_expanded() && !t->is_leaf())
			t = t->select();
		else
			break;
	}

	std::cerr << std::endl;
}

move mcts_ai::get_best_move(const position& pos) const {
	tree* root = tree::create(0, invalid_move, nullptr, pos);
	int max_time = m_timer->get_max_time_for_move(pos);
	m_timer->start();
	m_timer->set_alarm(max_time);
	int iter = 0;
	time_over = false;
	while (iter < ms_max_iterations) {
		tree* t = root;
		tree* first = nullptr;
		position tmp = pos;

		//selection
		while (t->is_fully_expanded() && !t->is_leaf()) {
			t = t->select();
			tmp.do_move(t->get_move());
			if (first == nullptr) {
				first = t;
			}
		}

		//expansion
		if (!t->is_fully_expanded()) {
			t = t->expand(tmp);
		}

		//simple play outs
		double score = 0.0;

		for (int s = 0; s < ms_max_samples; ++s) {
			auto sample_pos = tmp;
			for (move m = sample_pos.get_random_move(); m != invalid_move; m = sample_pos.get_random_move()) {
				sample_pos.do_move(m);
			}

			player winner = sample_pos.get_player();
			if (winner == pos.get_player())
				score += 1.0 - 0.001 * sample_pos.get_moves();
			else
				score += 0.001 * sample_pos.get_moves();
		}

		score /= ms_max_samples;

		while (t != nullptr) {
			t->update(pos.get_player(), score, ms_max_samples);
			t = t->get_parent();
		}

		if (time_over) {
			break;
		}

		++iter;
	}

	m_timer->stop();
	m_timer->clear_alarm();

	int delta_time = m_timer->get_delta_time();
	int total_time = m_timer->get_total_time();

	tree* best_child = root->select_uct_child();

	if (delta_time != 0)
		ratio = 1000 * ms_max_samples * iter / delta_time;

	std::cerr.setf(std::ios::fixed);
	std::cerr.precision(3);
	std::cerr <<
		"c = " << root->get_children_count() << std::endl <<
		"v = " << best_child->get_value() <<
		", n = " << best_child->get_visits() <<
		", mt = " << max_time <<
		", dt = " << delta_time <<
		", tt = " << total_time <<
		", i = " << iter <<
		", r = " << ratio <<
		std::endl;

	print_variation(best_child);

	move best_move = best_child->get_move();

	tree::clear_allocated_trees();

	return best_move;
}
