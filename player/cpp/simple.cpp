#include "common.h"

#include "ai.h"
#include "position.h"

struct data {
	data(const move& mn) : w(0), c(0), m(mn) {}
	int w;
	int c;
	move m;
};

move simple_ai::get_best_move(const position& pos) const {
	int max_time = m_timer->get_max_time_for_move(pos);
	m_timer->start();
	m_timer->set_alarm(max_time);
	time_over = false;
	int ts = 0;
	move_list moves;
	pos.get_possible_moves(moves);

	std::list<data> datas;
	for(auto it = moves.begin(), end = moves.end(); it != end; ++it)
		datas.push_back(data(*it));

	auto it = datas.begin(), end = datas.end();
	
	while(!time_over && ts < 4000) {
		auto tmp = pos;
		tmp.do_move(it->m);
		for(auto m = tmp.get_random_move(); m != invalid_move; m = tmp.get_random_move())
			tmp.do_move(m);
		if(tmp.get_player() == pos.get_player())
			it->w += 1000 - tmp.get_moves();
		it->c += 1;

		++ts;
		++it;
		if(it == end)
			it = datas.begin();
	}

	m_timer->stop();
	m_timer->clear_alarm();

	auto best_move = invalid_move;
	auto best_value = -OO;

	for(it = datas.begin(); it != end; ++it) {
		auto value = it->w;
		value /= it->c;
		if(best_value < value) {
			best_value = value;
			best_move = it->m;
		}
	}

	std::cerr << "c = " << datas.size() << std::endl <<
		"v = " << best_value <<
		", dt = " << m_timer->get_delta_time() <<
		", tt = " << m_timer->get_total_time() <<
		", ts = " << ts << std::endl;	

	return best_move;
}
