#ifndef __AI_H__
#define __AI_H__

#include "move.h"
#include "position.h"
#include "timer.h"

class tree;

class ai {
public:
	virtual move get_best_move(const position& pos) const = 0;
};

class random_ai : public ai {
public:
	move get_best_move(const position& pos) const {
		move random_move = pos.get_random_move();
		return random_move;
	}
};

class simple_ai : public ai {
public:
	simple_ai() : m_timer(new timer()) {}
	~simple_ai() { delete m_timer; }
	
	move get_best_move(const position& pos) const;

private:
	timer* m_timer;
};

class mcts_ai : public ai {
public:
	mcts_ai() : m_timer(new timer()) {}
	~mcts_ai() { delete m_timer; }
    
	move get_best_move(const position& pos) const;

private:
	void print_variation(const tree* t) const;

    timer* m_timer;
	static int ms_max_iterations;
	static int ms_max_samples;
};

#endif
