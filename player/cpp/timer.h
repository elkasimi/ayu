#ifndef __TIMER_H__
#define __TIMER_H__

class position;

class timer {
public:
	timer();
	void start();
	void stop();
	int get_delta_time();
	int get_total_time();
	int get_max_time_for_move(const position& pos);
	void set_alarm(int t);
	void clear_alarm();
private:
	int m_start;
	int m_end;
	int m_total_time;
	static int ms_max_time;
};

extern volatile bool time_over;

#endif
