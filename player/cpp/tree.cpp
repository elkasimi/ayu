#include "common.h"

#include "tree.h"
#include "position.h"
#include "util.h"

std::list<tree*> tree::ms_allocated_trees;

double tree::ms_uctk = 0.0;

tree::tree() {}

tree* tree::create(int level, const move& m, tree* parent, const position& pos) {
	tree* t = new tree();

    t->m_move = m;
    t->m_parent = parent;

    //init MC statistics
    t->m_value = 0.0;
    t->m_visits = 0;

    //init player
    t->m_player = 1 - pos.get_player();

    //init tree level
    t->m_level = level;

	pos.get_possible_moves(t->m_untried_moves);

	t->m_is_leaf = (t->m_untried_moves.empty());

	ms_allocated_trees.push_back(t);

    return t;
}

void tree::clear_allocated_trees() {
	for (auto it = ms_allocated_trees.begin(), end = ms_allocated_trees.end(); it != end; ++it) {
		delete (*it);
	}

	ms_allocated_trees.clear();
}

tree* tree::expand(position& pos) {
	int len = m_untried_moves.size();
	int r = rand() % len;
	auto it = m_untried_moves.begin();
	std::advance(it, r);

	move random_move = *it;
	pos.do_move(random_move);

    tree* new_child = create(m_level + 1, random_move, this, pos);

    //remove move from tree's untried moves list
	m_untried_moves.erase(it);

    //add the new child to the tree children list
    m_children.push_back(new_child);

    return new_child;
}

double tree::get_uct_value() const {
    double value = m_value;
	value += ms_uctk * sqrt(log((double)m_parent->m_visits) / m_visits);

    return value;
}

tree* tree::select() const {
	auto child = select_random_child();
	return child;
}

tree* tree::select_random_child() const {
	auto it = util::get_random_iterator(m_children);
	auto child = *it;

	return child;
}

tree* tree::select_uct_child() const {
    tree* selected = nullptr;
    double best_value = -OO;

	for (auto it = m_children.begin(), end = m_children.end(); it != end; ++it) {
		auto child = (*it);
		auto value = child->get_uct_value();

        if (best_value < value) {
            best_value = value;
            selected = child;
        }
    }

    return selected;
}

tree* tree::select_most_visited_child() const {
    tree* selected = nullptr;
    int max_visits = 0;

	for (auto it = m_children.begin(), end = m_children.end(); it != end; ++it) {
		auto child = (*it);

        if (max_visits < child->m_visits) {
            max_visits = child->m_visits;
            selected = child;
        }
    }

    return selected;
}

void tree::update(player p, double score, int count) {
	m_value = m_value * m_visits;
	if (m_player == p)
		m_value += score * count;
	else
		m_value += (1 - score) * count;

	m_value /= m_visits + count;

	m_visits += count;
}
