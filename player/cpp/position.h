#ifndef __POSITION__
#define __POSITION__

#include "move.h"
#include "group.h"

class position {
public:
	position();

	void do_move(const move& m);
	void undo_move(const move& m);
	void get_possible_moves(move_list& moves) const;
	move get_random_move() const;
	int get_moves() const {
		return m_moves;
	}
	player get_owner(int f) const {
		return m_owner[f];
	}
	player get_player() const {
		return m_player;
	}
	bool is_empty(int f) const {
		return (m_owner[f] == none);
	}
	void get_fields_owner(player owner[NXN]) const {
		memcpy(owner, m_owner, NXN * sizeof(player));
	}

private:
	void change_player() { m_player = 1 - m_player; }
	void get_curr_player_groups(group_list& groups) const;

	player m_player;
	player m_owner[NXN];
	int m_moves;
};

#endif
