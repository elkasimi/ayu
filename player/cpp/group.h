#ifndef __GROUP_H__
#define __GROUP_H__

#include "move.h"

class position;

class group {
public:
	group(player owner) : m_owner(owner) {
		memset(m_neighbors, 0x0, NXN);
	}

	void add(int f);
	void remove(int f);
	void merge_with(const group& g);
	bool contains(int f) const;
	bool has_neighbor(int f) const;
	bool check_move(int from, int to, const position* pos) const;
	void get_possible_moves(move_list& moves, const position* pos) const;
	move get_random_move(const position* pos) const;
	int get_members(int members[MAX_MEMBERS]) const {
		int len = 0;
		for (auto it = m_members.begin(), end = m_members.end(); it != end; ++it) {
			members[len++] = *it;
		}
		return len;
	}
	int get_count() const {
		int c = (int) m_members.size();
		return c;
	}

private:
	player m_owner;
	int m_neighbors[NXN];
	std::set<int> m_members;
};

typedef std::vector<group> group_list;

#endif
