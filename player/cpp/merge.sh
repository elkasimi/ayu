#!/bin/bash
cat common.h     \
	data.h       \
	move.h       \
	queue.h      \
	group.h      \
	position.h   \
	timer.h      \
	tree.h       \
	util.h       \
	ai.h 		 \
	common.cpp   \
	data.cpp     \
	group.cpp    \
	simple.cpp   \
	mcts.cpp     \
	move.cpp     \
	position.cpp \
	timer.cpp    \
	tree.cpp     \
	util.cpp     \
	main.cpp > mamay.cpp

sed -i 's/.*include ".*"//' mamay.cpp
