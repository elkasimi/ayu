var N = 11;
var R = 40;
var A = 2*R;
var WIDTH = A * (N-1);
var HEIGHT = A * (N-1);
var OFFSET_X = 2.5 * R;
var OFFSET_Y = 2.5 * R;
var OFFSET_COORDINATE = A;



var arena = null;
var ctx = null;
var firstPlayerUnits = null;
var secondPlayerUnits = null;
var possibilities = null;
var selected = null;

function drawBackground() {
	ctx.fillStyle = "gray";
	ctx.fillRect(0, 0, arena.width, arena.height);
	ctx.fill();
}

function drawGrid() {
	ctx.strokeStyle = "yellow";
	
	for(var i = 0; i < N; ++i) {
		ctx.beginPath();
		ctx.moveTo(OFFSET_X, A*i + OFFSET_Y);
		ctx.lineTo(OFFSET_X + (N-1)*A, A*i + OFFSET_Y);
		ctx.stroke();
	}
	
	for(var i = 0; i < N; ++i) {
		ctx.beginPath();
		ctx.moveTo(A*i + OFFSET_X, OFFSET_Y);
		ctx.lineTo(A*i + OFFSET_X, (N-1)*A + OFFSET_Y);
		ctx.stroke();
	}	
}

function drawCoordinates() {
	var coordinates = 'ABCDEFGHIJK';
	ctx.fillStyle = "yellow";
	for(var i = 0; i < N; ++i) {
		ctx.fillText(coordinates[i], A*i + OFFSET_X, N*A + OFFSET_COORDINATE);
		ctx.fill();
	}
	
	for(var i = 0; i < N; ++i) {
		var n = N - i;
		var t = n < (N-1) ? " " + n : n;
		ctx.fillText(t, OFFSET_X - OFFSET_COORDINATE, i*A + OFFSET_Y);
		ctx.fill();
	}	
}

function drawUnits() {
	
	firstPlayerUnits.forEach(function(u) {
		var u_x = u.x * A + OFFSET_X;
		var u_y = u.y * A + OFFSET_Y;
		ctx.beginPath();
		ctx.fillStyle = "rgba(255, 0, 0, 0.8)";
		ctx.arc(u_x, u_y, R, 0, 2 * Math.PI, false);
		ctx.fill();
		
		ctx.beginPath();
		ctx.strokeStyle = "black";
		ctx.arc(u_x, u_y, R, 0, 2 * Math.PI, false);
		ctx.stroke();
	});
	
	secondPlayerUnits.forEach(function(u) {
		var u_x = u.x * A + OFFSET_X;
		var u_y = u.y * A + OFFSET_Y;
		ctx.beginPath();
		ctx.fillStyle = "rgba(255, 255, 255, 0.8)";
		ctx.arc(u_x, u_y, R, 0, 2 * Math.PI, false);
		ctx.fill();
		
		ctx.beginPath();
		ctx.strokeStyle = "black";
		ctx.arc(u_x, u_y, R, 0, 2 * Math.PI, false);
		ctx.stroke();	
	});	
}

function drawPossibilities() {
}

function onArenaClick() {
}

function showPossibilities(u) {
}

function hidePossibilities() {
}

function doMove(move) {
}

function undoMove(move) {
}

function init() {
	arena = document.getElementById('arena');
	arena.width = WIDTH + 2 * OFFSET_X;
	arena.height = HEIGHT + 2 * OFFSET_Y;
	
	ctx = arena.getContext('2d');
	
	firstPlayerUnits = [];
	secondPlayerUnits = [];
	
	for(var i = 0; i < N; ++i) {
		var maxj = i % 2 == 0 ? Math.floor(N/2) : Math.floor(N/2 + 1);

		
		for(var j = 0; j < maxj; ++j) {
			var x = (i % 2 == 0) ? ((2*j + 1) * A) : (2*j*A);
			var y = i * A;
			
			if(i%2 == 0)
				firstPlayerUnits.push({
					x : Math.round(x / A),
					y : Math.round(y / A)
				});
			else
				secondPlayerUnits.push({
					x : Math.round(x / A),
					y : Math.round(y / A)
				});
		}
	}
	
	drawBackground();
	drawGrid();
	drawCoordinates();
	drawUnits();
}

window.onload = init;
