#include "common.h"

#include "position.h"
#include "move.h"
#include "group.h"
#include "queue.h"
#include "data.h"

position::position() :
m_player(white),
m_moves(0) {
	for (int f = 0; f < NXN; ++f) {
		m_owner[f] = none;
	}

	for (int i = 0; i < N; ++i) {
		int j0;
		player p;

		if (i % 2 == 0) {
			j0 = 1;
			p = white;
		}
		else {
			j0 = 0;
			p = black;
		}

		for (int j = j0; j < N; j += 2) {
			int f = N * i + j;
			m_owner[f] = p;
		}
	}
}

void position::do_move(const move& m) {
	int from = m.from;
	int to = m.to;
	m_owner[from] = none;
	m_owner[to] = m_player;

	change_player();

	++m_moves;
}

void position::undo_move(const move& m) {
	int from = m.from;
	int to = m.to;
	change_player();
	m_owner[from] = m_player;
	m_owner[to] = none;
	--m_moves;
}

move position::get_random_move() const {
	move random_move = invalid_move;
	group_list groups;
	get_curr_player_groups(groups);

	int choices[30];
	int len = (int)groups.size();
	for (int i = 0; i < len; ++i) {
		choices[i] = i;
	}

	for (int count = len; count > 0;) {
		int r = rand() % count;
		int idx = choices[r];
		random_move = groups[idx].get_random_move(this);
		if (random_move != invalid_move)
			break;
		else {
			choices[r] = choices[--count];
		}
	}

	return random_move;
}

void position::get_possible_moves(move_list& moves) const {
	group_list groups;
	get_curr_player_groups(groups);

	for (auto it = groups.begin(), end = groups.end(); it != end; ++it)
		it->get_possible_moves(moves, this);
}

void position::get_curr_player_groups(group_list& groups) const {
	bool marked[NXN];

	memset(marked, 0x0, NXN);

	for (int f = 0; f < NXN; ++f) {
		if (m_owner[f] != m_player) {
			continue;
		}

		if (marked[f]) {
			continue;
		}

		group g(m_player);

		queue q;
		q.push(f);
		g.add(f);
		marked[f] = true;

		while (!q.empty()) {
			int t = q.pop();
			for (int* n = neighbors[t]; *n != END; ++n) {
				if (!marked[*n] && m_owner[*n] == m_player) {
					q.push(*n);
					g.add(*n);
					marked[*n] = true;
				}
			}
		}

		groups.push_back(g);
	}
}
