#include "common.h"

#include "timer.h"
#include "position.h"

int timer::ms_max_time = 30000;

volatile bool time_over;

#if WINDOWS_VERSION
#else
typedef struct sigaction handler_t;
handler_t old_handler, new_handler;
#endif


timer::timer() : m_start(0), m_end(0), m_total_time(0) {}

void timer::start() {
    m_start = 1000 * clock() / CLOCKS_PER_SEC;
}

void timer::stop() {
    m_end = 1000 * clock() / CLOCKS_PER_SEC;
    m_total_time += m_end - m_start;
}

int timer::get_delta_time() {
    int delta_time = m_end - m_start;

    return delta_time;
}

int timer::get_total_time() {
    return m_total_time;
}

int timer::get_max_time_for_move(const position& pos) {
    int remaining_moves = 70 - pos.get_moves();
    remaining_moves /= 2;
    if (remaining_moves <= 0) {
        remaining_moves = 1;
    }

    int max_time = (ms_max_time - m_total_time);
    max_time /= remaining_moves;

    if(max_time > 1000)
        max_time = 1000;

    if(m_total_time > 28000)
        max_time = 100;

    return max_time;
}

void alarm_callback(int sig) {
	time_over = true;
}

void timer::set_alarm(int t) {
#if WINDOWS_VERSION
#else
    struct itimerval new_timer, old_timer;

    //Install signal handler
    new_handler.sa_handler = alarm_callback;
    sigaction(SIGALRM, &new_handler, &old_handler);

    //Set timer with setitimer
    new_timer.it_interval.tv_sec = 0;
    new_timer.it_interval.tv_usec = 0;
    new_timer.it_value.tv_sec = (time_t)(t / 1000);
    new_timer.it_value.tv_usec = (suseconds_t)((t % 1000) * 1000);
    setitimer(ITIMER_REAL, &new_timer, &old_timer);
#endif
}

void timer::clear_alarm() {
#if WINDOWS_VERSION
#else
    struct itimerval stop_timer = { { 0, 0 }, { 0, 0 } };
    setitimer(ITIMER_REAL, &stop_timer, NULL);
    sigaction(SIGALRM, &old_handler, NULL);
#endif
}
