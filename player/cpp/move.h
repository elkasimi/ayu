#ifndef __MOVE_H__
#define __MOVE_H__

struct move {
	move() : from(0), to(0) {}
	move(int f, int t) : from(f), to(t) {}
	int from;
	int to;
};

std::ostream& operator <<(std::ostream& out, const move& m);

std::istringstream& operator >>(std::istringstream& in, move& m);

bool operator != (const move& lhs, const move& rhs);

typedef std::vector<move> move_list;

extern const move invalid_move;

#endif
