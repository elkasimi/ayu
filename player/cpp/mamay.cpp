#ifndef __COMMON_H__
#define __COMMON_H__

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <list>
#include <vector>
#include <set>
#include <ctime>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <cassert>

#if WINDOWS_VERSION
#else
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#endif

typedef unsigned char player;
const player white = 0;
const player black = 1;
const player none = 2;

typedef unsigned long long ulong64;

#define N 11
#define NXN 121
#define END (-1)
#define OO 1000000000

#define MAX_MOVES 100
#define MAX_MEMBERS 30

#endif
#ifndef __DATA_H__
#define __DATA_H__

extern int neighbors[NXN][5];

#endif
#ifndef __MOVE_H__
#define __MOVE_H__

struct move {
	move() : from(0), to(0) {}
	move(int f, int t) : from(f), to(t) {}
	int from;
	int to;
};

std::ostream& operator <<(std::ostream& out, const move& m);

std::istringstream& operator >>(std::istringstream& in, move& m);

bool operator != (const move& lhs, const move& rhs);

typedef std::vector<move> move_list;

extern const move invalid_move;

#endif
#ifndef __QUEUE_H__
#define __QUEUE_H__

#define MAX_QUEUE_SIZE 1024

class queue {
public:
	queue() : m_start(0), m_end(0) {}
	int pop() { return m_a[m_start++]; }
	void push(int val) { m_a[m_end++] = val; }
	bool empty() { return (m_start == m_end); }
private:
	int m_start;
	int m_end;
	int m_a[MAX_QUEUE_SIZE];
};

#endif
#ifndef __GROUP_H__
#define __GROUP_H__



class position;

class group {
public:
	group(player owner) : m_owner(owner) {
		memset(m_neighbors, 0x0, NXN);
	}

	void add(int f);
	void remove(int f);
	void merge_with(const group& g);
	bool contains(int f) const;
	bool has_neighbor(int f) const;
	bool check_move(int from, int to, const position* pos) const;
	void get_possible_moves(move_list& moves, const position* pos) const;
	move get_random_move(const position* pos) const;
	int get_members(int members[MAX_MEMBERS]) const {
		int len = 0;
		for (auto it = m_members.begin(), end = m_members.end(); it != end; ++it) {
			members[len++] = *it;
		}
		return len;
	}
	int get_count() const {
		int c = (int) m_members.size();
		return c;
	}

private:
	player m_owner;
	int m_neighbors[NXN];
	std::set<int> m_members;
};

typedef std::vector<group> group_list;

#endif
#ifndef __POSITION__
#define __POSITION__




class position {
public:
	position();

	void do_move(const move& m);
	void undo_move(const move& m);
	void get_possible_moves(move_list& moves) const;
	move get_random_move() const;
	int get_moves() const {
		return m_moves;
	}
	player get_owner(int f) const {
		return m_owner[f];
	}
	player get_player() const {
		return m_player;
	}
	bool is_empty(int f) const {
		return (m_owner[f] == none);
	}
	void get_fields_owner(player owner[NXN]) const {
		memcpy(owner, m_owner, NXN * sizeof(player));
	}

private:
	void change_player() { m_player = 1 - m_player; }
	void get_curr_player_groups(group_list& groups) const;

	player m_player;
	player m_owner[NXN];
	int m_moves;
};

#endif
#ifndef __TIMER_H__
#define __TIMER_H__

class position;

class timer {
public:
	timer();
	void start();
	void stop();
	int get_delta_time();
	int get_total_time();
	int get_max_time_for_move(const position& pos);
	void set_alarm(int t);
	void clear_alarm();
private:
	int m_start;
	int m_end;
	int m_total_time;
	static int ms_max_time;
};

extern volatile bool time_over;

#endif
#ifndef __TREE_H__
#define __TREE_H__



class position;
struct move;

class tree {
public:
	static tree* create(int level, const move& m, tree* parent, const position& pos);

	static void clear_allocated_trees();

	bool is_fully_expanded() const {
		return m_untried_moves.empty();
	}

	bool is_leaf() const {
		return m_is_leaf;
	}
	
	tree* expand(position& pos);
	
	tree* select() const;
	
	tree* select_most_visited_child() const;

	tree* select_random_child() const;

	tree* select_uct_child() const;
	
	double get_value() const {
		return m_value;
	}
	
	double get_uct_value() const;
	
	void update(player p, double value, int count);

	int get_children_count() const {
		return (int)m_children.size();
	}

	const move& get_move() const {
		return m_move;
	}

	tree* get_parent() const {
		return m_parent;
	}

	int get_visits() const {
		return m_visits;
	}

private:
	tree();
	tree(const tree& t);
	tree& operator =(const tree& t);
	static std::list<tree*> ms_allocated_trees;
	static double ms_uctk;

	//the move that got us to this tree
	move m_move;

	//the parent tree NULL for root tree
	tree* m_parent;

	//tree children
	std::list<tree*> m_children;

	//Monte Carlo statistics
	double m_value;
	int m_visits;

	//untried moves count
	move_list m_untried_moves;

	//player just moved
	player m_player;

	//to know tree level
	int m_level;

	//to know if tree is leaf or not
	bool m_is_leaf;
};

#endif
#ifndef __UTIL_H__
#define __UTIL_H__

#include <list>

class util {
public:
	static void init_random_seed();
	static void display_first_error();

	template <class T>
	static typename std::list<T>::iterator get_random_iterator(std::list<T>& l) {
		auto i = l.end();
		
		if (!l.empty()) {
			int len = (int)l.size();
			i = l.begin();
			int r = rand() % len;
			std::advance(i, r);
		}

		return i;
	}

	template <class T>
	static typename std::list<T>::const_iterator get_random_iterator(const std::list<T>& l) {
		auto i = l.end();
		
		if (!l.empty()) {
			int len = (int)l.size();
			i = l.begin();
			int r = rand() % len;
			std::advance(i, r);
		}

		return i;
	}	
};

#endif
#ifndef __AI_H__
#define __AI_H__





class tree;

class ai {
public:
	virtual move get_best_move(const position& pos) const = 0;
};

class random_ai : public ai {
public:
	move get_best_move(const position& pos) const {
		move random_move = pos.get_random_move();
		return random_move;
	}
};

class simple_ai : public ai {
public:
	simple_ai() : m_timer(new timer()) {}
	~simple_ai() { delete m_timer; }
	
	move get_best_move(const position& pos) const;

private:
	timer* m_timer;
};

class mcts_ai : public ai {
public:
	mcts_ai() : m_timer(new timer()) {}
	~mcts_ai() { delete m_timer; }
    
	move get_best_move(const position& pos) const;

private:
	void print_variation(const tree* t) const;

    timer* m_timer;
	static int ms_max_iterations;
	static int ms_max_samples;
};

#endif





int neighbors[NXN][5] = {
    { 11, 1, END },
    { 0, 12, 2, END },
    { 1, 13, 3, END },
    { 2, 14, 4, END },
    { 3, 15, 5, END },
    { 4, 16, 6, END },
    { 5, 17, 7, END },
    { 6, 18, 8, END },
    { 7, 19, 9, END },
    { 8, 20, 10, END },
    { 9, 21, END },
    { 0, 22, 12, END },
    { 1, 11, 23, 13, END },
    { 2, 12, 24, 14, END },
    { 3, 13, 25, 15, END },
    { 4, 14, 26, 16, END },
    { 5, 15, 27, 17, END },
    { 6, 16, 28, 18, END },
    { 7, 17, 29, 19, END },
    { 8, 18, 30, 20, END },
    { 9, 19, 31, 21, END },
    { 10, 20, 32, END },
    { 11, 33, 23, END },
    { 12, 22, 34, 24, END },
    { 13, 23, 35, 25, END },
    { 14, 24, 36, 26, END },
    { 15, 25, 37, 27, END },
    { 16, 26, 38, 28, END },
    { 17, 27, 39, 29, END },
    { 18, 28, 40, 30, END },
    { 19, 29, 41, 31, END },
    { 20, 30, 42, 32, END },
    { 21, 31, 43, END },
    { 22, 44, 34, END },
    { 23, 33, 45, 35, END },
    { 24, 34, 46, 36, END },
    { 25, 35, 47, 37, END },
    { 26, 36, 48, 38, END },
    { 27, 37, 49, 39, END },
    { 28, 38, 50, 40, END },
    { 29, 39, 51, 41, END },
    { 30, 40, 52, 42, END },
    { 31, 41, 53, 43, END },
    { 32, 42, 54, END },
    { 33, 55, 45, END },
    { 34, 44, 56, 46, END },
    { 35, 45, 57, 47, END },
    { 36, 46, 58, 48, END },
    { 37, 47, 59, 49, END },
    { 38, 48, 60, 50, END },
    { 39, 49, 61, 51, END },
    { 40, 50, 62, 52, END },
    { 41, 51, 63, 53, END },
    { 42, 52, 64, 54, END },
    { 43, 53, 65, END },
    { 44, 66, 56, END },
    { 45, 55, 67, 57, END },
    { 46, 56, 68, 58, END },
    { 47, 57, 69, 59, END },
    { 48, 58, 70, 60, END },
    { 49, 59, 71, 61, END },
    { 50, 60, 72, 62, END },
    { 51, 61, 73, 63, END },
    { 52, 62, 74, 64, END },
    { 53, 63, 75, 65, END },
    { 54, 64, 76, END },
    { 55, 77, 67, END },
    { 56, 66, 78, 68, END },
    { 57, 67, 79, 69, END },
    { 58, 68, 80, 70, END },
    { 59, 69, 81, 71, END },
    { 60, 70, 82, 72, END },
    { 61, 71, 83, 73, END },
    { 62, 72, 84, 74, END },
    { 63, 73, 85, 75, END },
    { 64, 74, 86, 76, END },
    { 65, 75, 87, END },
    { 66, 88, 78, END },
    { 67, 77, 89, 79, END },
    { 68, 78, 90, 80, END },
    { 69, 79, 91, 81, END },
    { 70, 80, 92, 82, END },
    { 71, 81, 93, 83, END },
    { 72, 82, 94, 84, END },
    { 73, 83, 95, 85, END },
    { 74, 84, 96, 86, END },
    { 75, 85, 97, 87, END },
    { 76, 86, 98, END },
    { 77, 99, 89, END },
    { 78, 88, 100, 90, END },
    { 79, 89, 101, 91, END },
    { 80, 90, 102, 92, END },
    { 81, 91, 103, 93, END },
    { 82, 92, 104, 94, END },
    { 83, 93, 105, 95, END },
    { 84, 94, 106, 96, END },
    { 85, 95, 107, 97, END },
    { 86, 96, 108, 98, END },
    { 87, 97, 109, END },
    { 88, 110, 100, END },
    { 89, 99, 111, 101, END },
    { 90, 100, 112, 102, END },
    { 91, 101, 113, 103, END },
    { 92, 102, 114, 104, END },
    { 93, 103, 115, 105, END },
    { 94, 104, 116, 106, END },
    { 95, 105, 117, 107, END },
    { 96, 106, 118, 108, END },
    { 97, 107, 119, 109, END },
    { 98, 108, 120, END },
    { 99, 111, END },
    { 100, 110, 112, END },
    { 101, 111, 113, END },
    { 102, 112, 114, END },
    { 103, 113, 115, END },
    { 104, 114, 116, END },
    { 105, 115, 117, END },
    { 106, 116, 118, END },
    { 107, 117, 119, END },
    { 108, 118, 120, END },
    { 109, 119, END },
};










void group::add(int f) {
	m_members.insert(f);

	for (int* n = neighbors[f]; *n != END; ++n)
		m_neighbors[*n] += 1;
}

void group::remove(int f) {
	m_members.erase(f);

	for (int* n = neighbors[f]; *n != END; ++n)
		m_neighbors[*n] -= 1;
}

void group::merge_with(const group& g) {
	for (auto it = g.m_members.begin(), end = g.m_members.end(); it != end; ++it) {
		int m = *it;
		if (!contains(m)) {
			add(m);
		}
	}
}

bool group::contains(int f) const {
	return (m_members.find(f) != m_members.end());
}

bool group::has_neighbor(int f) const {
	return (m_neighbors[f] > 0);
}

void group::get_possible_moves(move_list& moves, const position* pos) const {
	//INIT MIND = OO
	//DO A BFS SEARCH
	//ADD ELEMENT WHEN ITS DISTANCE ISS LESS THAN MIND
	//WHENEVER YOU REACH A NEW GROUP
	//UPDATE MIND
	//MARK THE ORIGIN NEIGHBOUR AS A POSSIBLE MOVE
	int mind = OO;
	bool possible[NXN];
	int d[NXN], origin[NXN];
	queue q;

	for (int f = 0; f < NXN; ++f) {
		origin[f] = -1;
		possible[f] = false;
		if (contains(f)) {
			d[f] = 0;
			q.push(f);
		}
		else {
			d[f] = OO;
		}
	}

	while (!q.empty()) {
		int t = q.pop();

		if ((origin[t] == -1) && (d[t] == 1)) {
			origin[t] = t;
		}

		if (d[t] > mind) {
			continue;
		}

		for (int* n = neighbors[t]; *n != END; ++n) {
			if (pos->is_empty(*n) && (d[*n] >= d[t] + 1)) {
				d[*n] = d[t] + 1;
				q.push(*n);
				origin[*n] = origin[t];
			}

			if ((pos->get_owner(*n) == m_owner) && !contains(*n)) {
				//we reach a group
				int o = origin[t];
				if (o != -1) {
					possible[o] = true;
				}

				//update min distance
				if (mind > d[t]) {
					mind = d[t];
				}
			}
		}
	}

	for (int a = 0; a < NXN; ++a) {
		if (!possible[a])
			continue;

		for (auto it = m_members.begin(), end = m_members.end(); it != end; ++it) {
			int m = *it;
			int from = m;
			int to = a;
			if (check_move(from, to, pos))
				moves.push_back(move(from, to));
		}
	}
}

move group::get_random_move(const position* pos) const {
	move random_move = invalid_move;

	move_list moves;
	get_possible_moves(moves, pos);
	int len = (int)moves.size();

	if (len > 0) {
		int r = rand() % len;
		random_move = moves[r];
	}

	return random_move;
}

bool group::check_move(int from, int to, const position* pos) const {
	//no check is needed for unit groups
	if (m_members.size() == 1)
		return true;

	player owner[NXN];
	pos->get_fields_owner(owner);
	owner[from] = none;
	owner[to] = m_owner;

	int members[MAX_MEMBERS];
	int count = get_members(members);
	for (int i = 0; i < count; ++i) {
		if (members[i] == from) {
			members[i] = to;
			break;
		}
	}
	
	bool connected[NXN];
	memset(connected, false, NXN);
	int m0 = members[0];
	queue q;
	q.push(m0);
	connected[m0] = true;
	while (!q.empty()) {
		int t = q.pop();
		for (int* n = neighbors[t]; *n != END; ++n){
			if (!connected[*n] && (owner[*n] == m_owner)) {
				q.push(*n);
				connected[*n] = true;
			}
		}
	}

	bool ok = true;

	for (int i = 0; i < count; ++i) {
		int m = members[i];
		if (!connected[m]) {
			ok = false;
			break;
		}
	}

	return ok;
}





struct data {
	data(const move& mn) : w(0), c(0), m(mn) {}
	int w;
	int c;
	move m;
};

move simple_ai::get_best_move(const position& pos) const {
	int max_time = m_timer->get_max_time_for_move(pos);
	m_timer->start();
	m_timer->set_alarm(max_time);
	time_over = false;
	int ts = 0;
	move_list moves;
	pos.get_possible_moves(moves);

	std::list<data> datas;
	for(auto it = moves.begin(), end = moves.end(); it != end; ++it)
		datas.push_back(data(*it));

	auto it = datas.begin(), end = datas.end();
	
	while(!time_over && ts < 4000) {
		auto tmp = pos;
		tmp.do_move(it->m);
		for(auto m = tmp.get_random_move(); m != invalid_move; m = tmp.get_random_move())
			tmp.do_move(m);
		if(tmp.get_player() == pos.get_player())
			it->w += 1000 - tmp.get_moves();
		it->c += 1;

		++ts;
		++it;
		if(it == end)
			it = datas.begin();
	}

	m_timer->stop();
	m_timer->clear_alarm();

	auto best_move = invalid_move;
	auto best_value = -OO;

	for(it = datas.begin(); it != end; ++it) {
		auto value = it->w;
		value /= it->c;
		if(best_value < value) {
			best_value = value;
			best_move = it->m;
		}
	}

	std::cerr << "c = " << datas.size() << std::endl <<
		"v = " << best_value <<
		", dt = " << m_timer->get_delta_time() <<
		", tt = " << m_timer->get_total_time() <<
		", ts = " << ts << std::endl;	

	return best_move;
}







int ratio = 0;

int mcts_ai::ms_max_iterations = 100000;
int mcts_ai::ms_max_samples = 30;

void mcts_ai::print_variation(const tree* root) const {
	std::cerr << "ev = ";
	bool first = true;
	for (const tree* t = root;;) {
		if (first){
			first = false;
		}
		else {
			std::cerr << "=>";
		}

		std::cerr << t->get_move();

		if (t->is_fully_expanded() && !t->is_leaf())
			t = t->select();
		else
			break;
	}

	std::cerr << std::endl;
}

move mcts_ai::get_best_move(const position& pos) const {
	tree* root = tree::create(0, invalid_move, nullptr, pos);
	int max_time = m_timer->get_max_time_for_move(pos);
	m_timer->start();
	m_timer->set_alarm(max_time);
	int iter = 0;
	time_over = false;
	while (iter < ms_max_iterations) {
		tree* t = root;
		tree* first = nullptr;
		position tmp = pos;

		//selection
		while (t->is_fully_expanded() && !t->is_leaf()) {
			t = t->select();
			tmp.do_move(t->get_move());
			if (first == nullptr) {
				first = t;
			}
		}

		//expansion
		if (!t->is_fully_expanded()) {
			t = t->expand(tmp);
		}

		//simple play outs
		double score = 0.0;

		for (int s = 0; s < ms_max_samples; ++s) {
			auto sample_pos = tmp;
			for (move m = sample_pos.get_random_move(); m != invalid_move; m = sample_pos.get_random_move()) {
				sample_pos.do_move(m);
			}

			player winner = sample_pos.get_player();
			if (winner == pos.get_player())
				score += 1.0 - 0.001 * sample_pos.get_moves();
			else
				score += 0.001 * sample_pos.get_moves();
		}

		score /= ms_max_samples;

		while (t != nullptr) {
			t->update(pos.get_player(), score, ms_max_samples);
			t = t->get_parent();
		}

		if (time_over) {
			break;
		}

		++iter;
	}

	m_timer->stop();
	m_timer->clear_alarm();

	int delta_time = m_timer->get_delta_time();
	int total_time = m_timer->get_total_time();

	tree* best_child = root->select_uct_child();

	if (delta_time != 0)
		ratio = 1000 * ms_max_samples * iter / delta_time;

	std::cerr.setf(std::ios::fixed);
	std::cerr.precision(3);
	std::cerr <<
		"c = " << root->get_children_count() << std::endl <<
		"v = " << best_child->get_value() <<
		", n = " << best_child->get_visits() <<
		", mt = " << max_time <<
		", dt = " << delta_time <<
		", tt = " << total_time <<
		", i = " << iter <<
		", r = " << ratio <<
		std::endl;

	print_variation(best_child);

	move best_move = best_child->get_move();

	tree::clear_allocated_trees();

	return best_move;
}




const move invalid_move = { 0, 0 };

std::ostream& operator <<(std::ostream& out, const move& m) {
	int from = m.from;
	int to = m.to;
	out << ((char)('A' + (from%N))) << ((from / N) + 1) << "-" << ((char)('A' + (to%N))) << ((to / N) + 1);
	return out;
}

std::istringstream& operator >>(std::istringstream& in, move& m) {
	char c1, c2, minus;
	int n1, n2;

	in >> c1 >> n1 >> minus >> c2 >> n2;
	m.from = (c1 - 'A') + N * (n1 - 1);
	m.to = (c2 - 'A') + N * (n2 - 1);

	return in;
}

bool operator != (const move& lhs, const move& rhs) {
	return (lhs.from != rhs.from) || (lhs.to != rhs.to);
}








position::position() :
m_player(white),
m_moves(0) {
	for (int f = 0; f < NXN; ++f) {
		m_owner[f] = none;
	}

	for (int i = 0; i < N; ++i) {
		int j0;
		player p;

		if (i % 2 == 0) {
			j0 = 1;
			p = white;
		}
		else {
			j0 = 0;
			p = black;
		}

		for (int j = j0; j < N; j += 2) {
			int f = N * i + j;
			m_owner[f] = p;
		}
	}
}

void position::do_move(const move& m) {
	int from = m.from;
	int to = m.to;
	m_owner[from] = none;
	m_owner[to] = m_player;

	change_player();

	++m_moves;
}

void position::undo_move(const move& m) {
	int from = m.from;
	int to = m.to;
	change_player();
	m_owner[from] = m_player;
	m_owner[to] = none;
	--m_moves;
}

move position::get_random_move() const {
	move random_move = invalid_move;
	group_list groups;
	get_curr_player_groups(groups);

	int choices[30];
	int len = (int)groups.size();
	for (int i = 0; i < len; ++i) {
		choices[i] = i;
	}

	for (int count = len; count > 0;) {
		int r = rand() % count;
		int idx = choices[r];
		random_move = groups[idx].get_random_move(this);
		if (random_move != invalid_move)
			break;
		else {
			choices[r] = choices[--count];
		}
	}

	return random_move;
}

void position::get_possible_moves(move_list& moves) const {
	group_list groups;
	get_curr_player_groups(groups);

	for (auto it = groups.begin(), end = groups.end(); it != end; ++it)
		it->get_possible_moves(moves, this);
}

void position::get_curr_player_groups(group_list& groups) const {
	bool marked[NXN];

	memset(marked, 0x0, NXN);

	for (int f = 0; f < NXN; ++f) {
		if (m_owner[f] != m_player) {
			continue;
		}

		if (marked[f]) {
			continue;
		}

		group g(m_player);

		queue q;
		q.push(f);
		g.add(f);
		marked[f] = true;

		while (!q.empty()) {
			int t = q.pop();
			for (int* n = neighbors[t]; *n != END; ++n) {
				if (!marked[*n] && m_owner[*n] == m_player) {
					q.push(*n);
					g.add(*n);
					marked[*n] = true;
				}
			}
		}

		groups.push_back(g);
	}
}





int timer::ms_max_time = 30000;

volatile bool time_over;

#if WINDOWS_VERSION
#else
typedef struct sigaction handler_t;
handler_t old_handler, new_handler;
#endif


timer::timer() : m_start(0), m_end(0), m_total_time(0) {}

void timer::start() {
    m_start = 1000 * clock() / CLOCKS_PER_SEC;
}

void timer::stop() {
    m_end = 1000 * clock() / CLOCKS_PER_SEC;
    m_total_time += m_end - m_start;
}

int timer::get_delta_time() {
    int delta_time = m_end - m_start;

    return delta_time;
}

int timer::get_total_time() {
    return m_total_time;
}

int timer::get_max_time_for_move(const position& pos) {
    int remaining_moves = 70 - pos.get_moves();
    remaining_moves /= 2;
    if (remaining_moves <= 0) {
        remaining_moves = 1;
    }

    int max_time = (ms_max_time - m_total_time);
    max_time /= remaining_moves;

    if(max_time > 1000)
        max_time = 1000;

    if(m_total_time > 28000)
        max_time = 100;

    return max_time;
}

void alarm_callback(int sig) {
	time_over = true;
}

void timer::set_alarm(int t) {
#if WINDOWS_VERSION
#else
    struct itimerval new_timer, old_timer;

    //Install signal handler
    new_handler.sa_handler = alarm_callback;
    sigaction(SIGALRM, &new_handler, &old_handler);

    //Set timer with setitimer
    new_timer.it_interval.tv_sec = 0;
    new_timer.it_interval.tv_usec = 0;
    new_timer.it_value.tv_sec = (time_t)(t / 1000);
    new_timer.it_value.tv_usec = (suseconds_t)((t % 1000) * 1000);
    setitimer(ITIMER_REAL, &new_timer, &old_timer);
#endif
}

void timer::clear_alarm() {
#if WINDOWS_VERSION
#else
    struct itimerval stop_timer = { { 0, 0 }, { 0, 0 } };
    setitimer(ITIMER_REAL, &stop_timer, NULL);
    sigaction(SIGALRM, &old_handler, NULL);
#endif
}






std::list<tree*> tree::ms_allocated_trees;

double tree::ms_uctk = 0.0;

tree::tree() {}

tree* tree::create(int level, const move& m, tree* parent, const position& pos) {
	tree* t = new tree();

    t->m_move = m;
    t->m_parent = parent;

    //init MC statistics
    t->m_value = 0.0;
    t->m_visits = 0;

    //init player
    t->m_player = 1 - pos.get_player();

    //init tree level
    t->m_level = level;

	pos.get_possible_moves(t->m_untried_moves);

	t->m_is_leaf = (t->m_untried_moves.empty());

	ms_allocated_trees.push_back(t);

    return t;
}

void tree::clear_allocated_trees() {
	for (auto it = ms_allocated_trees.begin(), end = ms_allocated_trees.end(); it != end; ++it) {
		delete (*it);
	}

	ms_allocated_trees.clear();
}

tree* tree::expand(position& pos) {
	int len = m_untried_moves.size();
	int r = rand() % len;
	auto it = m_untried_moves.begin();
	std::advance(it, r);

	move random_move = *it;
	pos.do_move(random_move);

    tree* new_child = create(m_level + 1, random_move, this, pos);

    //remove move from tree's untried moves list
	m_untried_moves.erase(it);

    //add the new child to the tree children list
    m_children.push_back(new_child);

    return new_child;
}

double tree::get_uct_value() const {
    double value = m_value;
	value += ms_uctk * sqrt(log((double)m_parent->m_visits) / m_visits);

    return value;
}

tree* tree::select() const {
	auto child = select_random_child();
	return child;
}

tree* tree::select_random_child() const {
	auto it = util::get_random_iterator(m_children);
	auto child = *it;

	return child;
}

tree* tree::select_uct_child() const {
    tree* selected = nullptr;
    double best_value = -OO;

	for (auto it = m_children.begin(), end = m_children.end(); it != end; ++it) {
		auto child = (*it);
		auto value = child->get_uct_value();

        if (best_value < value) {
            best_value = value;
            selected = child;
        }
    }

    return selected;
}

tree* tree::select_most_visited_child() const {
    tree* selected = nullptr;
    int max_visits = 0;

	for (auto it = m_children.begin(), end = m_children.end(); it != end; ++it) {
		auto child = (*it);

        if (max_visits < child->m_visits) {
            max_visits = child->m_visits;
            selected = child;
        }
    }

    return selected;
}

void tree::update(player p, double score, int count) {
	m_value = m_value * m_visits;
	if (m_player == p)
		m_value += score * count;
	else
		m_value += (1 - score) * count;

	m_value /= m_visits + count;

	m_visits += count;
}




void util::init_random_seed() {
    unsigned long seed = (unsigned long)time(NULL);
    srand(seed);
}

void util::display_first_error() {
	std::cerr << "R mamay" << std::endl;
}






int main(int argc, char* argv[]) {
	if(argc > 1) {
		util::init_random_seed();
		position pos;
		move m;
		simple_ai a;
		for(int i = 1; i < argc; ++i) {
			std::istringstream iss(argv[i]);
			iss >> m;
			pos.do_move(m);
		}
		auto p = pos;
		std::cerr << (p.get_player() == white ? "white to move" : "black to move") << std::endl;
		for(auto r = p.get_random_move(); r != invalid_move; r = p.get_random_move()) {
			std::cerr << r << std::endl;
			p.do_move(r);
		}
		std::cerr << (p.get_player() == white ? "white wins" : "black wins") << std::endl;
	} else {
		position pos;
		std::string s;
		simple_ai a;

		//util::init_random_seed();
		//util::display_first_error();
	    
	    std::cerr << "tree size = " << sizeof(tree) << std::endl;
	    std::cerr << "position size = " << sizeof(position) << std::endl;

		std::cin >> s;
		while(s != "Quit") {
			if(s != "Start") {
				std::istringstream iss(s);
				move opp_move;
				iss >> opp_move;
				pos.do_move(opp_move);
				std::cerr << opp_move << std::endl;
			}
			move my_move = a.get_best_move(pos);
			pos.do_move(my_move);
			std::cerr << my_move << std::endl;
			std::cout << my_move << std::endl;
			std::cin >> s;
		}
	}

    return 0;
}
