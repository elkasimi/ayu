#include "common.h"
#include "position.h"
#include "ai.h"
#include "tree.h"
#include "util.h"

int main(int argc, char* argv[]) {
	if(argc > 1) {
		util::init_random_seed();
		position pos;
		move m;
		simple_ai a;
		for(int i = 1; i < argc; ++i) {
			std::istringstream iss(argv[i]);
			iss >> m;
			pos.do_move(m);
		}
		auto p = pos;
		std::cerr << (p.get_player() == white ? "white to move" : "black to move") << std::endl;
		for(auto r = p.get_random_move(); r != invalid_move; r = p.get_random_move()) {
			std::cerr << r << std::endl;
			p.do_move(r);
		}
		std::cerr << (p.get_player() == white ? "white wins" : "black wins") << std::endl;
	} else {
		position pos;
		std::string s;
		simple_ai a;

		//util::init_random_seed();
		//util::display_first_error();
	    
	    std::cerr << "tree size = " << sizeof(tree) << std::endl;
	    std::cerr << "position size = " << sizeof(position) << std::endl;

		std::cin >> s;
		while(s != "Quit") {
			if(s != "Start") {
				std::istringstream iss(s);
				move opp_move;
				iss >> opp_move;
				pos.do_move(opp_move);
				std::cerr << opp_move << std::endl;
			}
			move my_move = a.get_best_move(pos);
			pos.do_move(my_move);
			std::cerr << my_move << std::endl;
			std::cout << my_move << std::endl;
			std::cin >> s;
		}
	}

    return 0;
}
